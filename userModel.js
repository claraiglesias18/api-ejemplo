const mongoose = require('mongoose');

const userModel = mongoose.model('usuario', {

    nombre: { type: String, required: true, minLength: 3},
    apellidos: { type: String, required: true, minLength: 3},

})

module.exports = userModel;