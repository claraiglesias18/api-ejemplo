
const plantillaInicial = () => {

    const template = `
        <h1>Usuarios</h1>
        <form id="user-form">
            <div>
                <label>Nombre</label>
                <input name="nombre"/>
            </div>
            <div>
                <label>Apellidos</label>
                <input name="apellidos"/>
            </div>
            <div>
                <button type="submit">Enviar</button>
            </div>
        </form>
        <ul id="user-list"></ul>
    `;

    const body = document.getElementsByTagName('body')[0];

    body.innerHTML = template;

}

const getUsers = async () => {

    //pedir todos los usuarios a la bdd
    const respuesta = await fetch('/usuarios');
    const usuarios = await respuesta.json();

    //funcion para insertar elementos de lista HTML
    function pintar(usuario) {

        return `<li>${usuario.nombre} ${usuario.apellidos} <button data-id="${usuario._id}">Eliminar</li>`;

    }

    //se seleccion el elemento <ul>
    const lista = document.getElementById('user-list');

    //se toma la lista y se inserta cada elemetno del array de usuarios con la funcion de pintar    
    lista.innerHTML = usuarios.map(usuarios => pintar(usuarios)).join('');

    //agragamos la funcionalidad de borrado al boton de eliminar

    usuarios.forEach(usuarios => {
        const nodoBoton = document.querySelector(`[data-id="${usuarios._id}"]`)   // usamos querySelector porque data-id es una propiedad custom
        
        nodoBoton.onclick = async (e) =>{

            await fetch(`/usuarios/${usuarios._id}` , {
            
                method: 'DELETE',
            
            })
            
            nodoBoton.parentNode.remove() // sube al elemento <li> y lo borra
            alert('Eliminado con éxito')
        
        }
    });



}

const formListener = async () => {

    const formulario = document.getElementById('user-form');

    formulario.onsubmit = async (e) => {

        e.preventDefault();

        const datosForm = new FormData(formulario);

        const datos = Object.fromEntries(datosForm.entries());

        //console.log(datos);

        await fetch('usuarios', {

            method: 'POST',
            body: JSON.stringify(datos),
            headers: {
                'Content-type': 'application/json'
            }

        })
        
        getUsers();

    }

}



window.onload = () => {

    plantillaInicial();
    formListener();
    getUsers();

}
