//!modificar la funcion de actualizar (Object.assign)

const userModel = require('./userModel');
//const router = express.Router();

const userController = {
//crear usuario

    crearUsuario(req, res) {

    const user = userModel(req.body);
                
    user
        .save()
        .then((data) => {res.status(200).send(data)})
        .catch((error) => {res.status(500).send(error)});

    },

//listar usuarios
    listaUsuarios(req, res) {

    userModel
        .find()
        .then((data) => {res.status(200).send(data)})
        .catch((error) => {res.status(500).send(error)});

},

//listar un usuario
    listaPorId(req, res) {
    
    const { id } = req.params;
    userModel
        .findById(id)
        .then((data) => {res.status(200).send(data)})
        .catch((error) => {res.status(500).send(error)});

},

//actualizar un usuario
    actualizar(req, res) {
    
    const { id } = req.params;
    const { name, age, email } = req.body;

    userModel
        .updateOne({ _id: id }, { $set: {name, age, email} })
        .then((data) => {res.status(200).send(data)})
        .catch((error) => {res.status(500).send(error)});

},

//eliminar usuario
    eliminar(req, res) {
    
    const { id } = req.params;
    userModel
        .deleteOne({ _id: id })
        .then((data) => {res.status(200).send(data)})
        .catch((error) => {res.status(500).send(error)});

},

};

module.exports = userController;