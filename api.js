const express = require('express');
const mongoose = require('mongoose');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const port = process.env.PORT || 4000;
const userController = require('./userController');

app.use(express.json());

//listar usuarios
app.get('/usuarios/',userController.listaUsuarios);

//crear usuario
app.post('/usuarios/', userController.crearUsuario);  

//listar por ID
app.get('/usuarios/:id', userController.listaPorId)

//actualizar usuario
app.put('/usuarios/:id', userController.actualizar);
app.patch('/usuarios/:id/', userController.actualizar);

//eliminar usuario
app.delete('/usuarios/:id', userController.eliminar);

//conexion con el cliente
app.use(express.static('client'));
app.get('/', (req,res)=>{res.sendFile(`${__dirname}/index.html`)});

//error de peticion
app.get('*', (req,res) =>{
    res.status(404).send('La ruta especificada no existe.');
});

//conexion con mongoDB
mongoose
    .connect(process.env.MONGODB_URI)
    .then(() => console.log('Conectado con MongoDB Atlas'));

app.listen(port, () => console.log('Escuchando por el puerto' , port));

